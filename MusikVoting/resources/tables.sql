CREATE TABLE IF NOT EXISTS T_Users(
P_User_name VARCHAR(30) PRIMARY KEY,
nobility_house VARCHAR(30),
password_hash BINARY(64),
password_salt BINARY(32)
); 
CREATE TABLE IF NOT EXISTS T_Songs(
P_Song_id INT PRIMARY KEY AUTO_INCREMENT,
genre VARCHAR(30) NOT NULL,
title VARCHAR(100) NOT NULL,
band VARCHAR(50) NOT NULL,
F_User_name VARCHAR(30) NOT NULL,
FOREIGN KEY (F_User_name) REFERENCES T_Users(P_User_name)
);
CREATE TABLE IF NOT EXISTS T_Votes(
P_Vote_id INT AUTO_INCREMENT PRIMARY KEY,
F_Song_id INT,
F_User_name VARCHAR(30),
FOREIGN KEY (F_Song_id) REFERENCES T_Songs(P_Song_id),    
FOREIGN KEY (F_User_name) REFERENCES T_Users(P_User_name)
);
