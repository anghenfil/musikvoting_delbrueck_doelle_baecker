package main;

import java.awt.EventQueue;

import GUI.Window;
import configuration.ConfigManager;
import database.Setup;

public class MusikVoting {

	public static void main(String[] args) {
		if(!ConfigManager.load("musikvoting.properties")) {
			System.err.println("Configfile musikvoting.properties couldn't loaded.");
			System.exit(1);
		}
		
		Setup st = new Setup();
		st.createDatabase();
		if(!st.createDatabase()) {
			System.err.println("Couldn't setup database. Please double check if database server is running.");
			System.exit(1);
		}
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window frame = new Window();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		
	}

}
