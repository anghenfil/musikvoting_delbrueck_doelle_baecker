package logic;

public class Song {
	 
    private int ID;
    private String genre;
    private String band;
    private String title;
    private int votes;

	public Song(int ID, String genre, String band, String title, int votes) {
		super();
		this.ID = ID;
		this.genre = genre;
		this.band = band;
		this.title = title;
		this.votes = votes;
	}
	
	public Song(int ID, String genre, String band, String title) {
		super();
		this.ID = ID;
		this.genre = genre;
		this.band = band;
		this.title = title;
	}
	
	public Song(String genre, String band, String title) {
		super();
		this.genre = genre;
		this.band = band;
		this.title = title;
	}
	
	public int getID() {
		return ID;
	}

	public String getGenre() {
		return genre;
	}

	public String getBand() {
		return band;
	}

	public String getTitle() {
		return title;
	}
	
	public int getVotes() {
		return votes;
	}

	public void setVotes(int votes) {
		this.votes = votes;
	}
}
