package logic;

public class Vote {

	private String user;
	private int songID;
	
	public Vote(int songID, String user) {
		this.songID = songID;
		this.user = user;
	}

	public int getSongID() {
		return songID;
	}

	public String getUser() {
		return user;
	}
}
