package logic;

public class User {
	private String name;
	private String nobilityHouse;
	private String passwordHash;
	private String passwordSalt;
	

	public User(String name, String nobilityHouse) {
		super();
		this.name = name;
		this.nobilityHouse = nobilityHouse;
	}
	
	public User(String name, String nobilityHouse, String passwordHash, String passwordSalt) {
		super();
		this.name = name;
		this.nobilityHouse = nobilityHouse;
		this.passwordHash = passwordHash;
		this.passwordSalt = passwordSalt;
	}

	public String getName() {
		return name;
	}

	public String getNobilityHouse() {
		return nobilityHouse;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public String getPasswordSalt() {
		return passwordSalt;
	}
}
