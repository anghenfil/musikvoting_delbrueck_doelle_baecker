package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import configuration.ConfigManager;
import logic.Song;
import logic.User;

public class UserSQL extends SQL{
	
	public boolean addUser(User user) {
		Connection con = null;
		PreparedStatement stmt = null;
		
		try {
			con = getConnection();
			con.setAutoCommit(false);
			con.setCatalog(ConfigManager.get("DB_DATABASE"));
			
			String query = "INSERT INTO T_Users(P_User_name, nobility_house) VALUES (?, ?)";
			
			stmt = con.prepareStatement(query);
			stmt.setString(1, user.getName());
			stmt.setString(2, user.getNobilityHouse());
			
			stmt.execute();
			con.commit();
			return true;
		}catch(SQLException e1) {
			try {
				con.rollback();
			} catch (SQLException e) {}
			return false;
		}finally {
			if(stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {}
			}
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {}
			}
		}
	}
	
	//checkUser returns null if user doesn't exist.
	public User checkUser(User user) {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			con.setCatalog(ConfigManager.get("DB_DATABASE"));
			
			String query = "Select P_User_name, nobility_house, password_hash, password_salt From T_Users WHERE P_User_name = ? AND nobility_house = ? ";
			
			stmt = con.prepareStatement(query);
			stmt.setString(1, user.getName());
			stmt.setString(2, user.getNobilityHouse());
			
			ResultSet rs = stmt.executeQuery();
			con.commit();
			if(!rs.next()) {
				System.out.println("User doesn't exists");
				return null;
			}else {			
				System.out.println("User exists");
				return new User(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4));
			}
		}catch(SQLException e1) {
			try {
				con.rollback();
				System.out.println(e1);
			} catch (SQLException e) {}
			return null;
		}
	}
}

