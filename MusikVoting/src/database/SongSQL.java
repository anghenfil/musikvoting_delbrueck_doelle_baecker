package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import configuration.ConfigManager;
import logic.Song;

public class SongSQL extends SQL{
	
	public boolean addSong(Song song, String user) {
		Connection con = null;
		PreparedStatement stmt = null;
		
		try {
			con = getConnection();
			con.setAutoCommit(false);
			con.setCatalog(ConfigManager.get("DB_DATABASE"));
			
			String query = "INSERT INTO T_Songs(genre, title, band, F_User_name) VALUES (?, ?, ?, ?)";
			
			stmt = con.prepareStatement(query);
			stmt.setString(1, song.getGenre());
			stmt.setString(2, song.getTitle());
			stmt.setString(3, song.getBand());
			stmt.setString(4, user);
			
			stmt.execute();
			con.commit();
			return true;
		}catch(SQLException e1) {
			try {
				con.rollback();
			} catch (SQLException e) {}
			return false;
		}finally {
			if(stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {}
			}
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {}
			}
		}
	}
	
	public Song getNextSong(int actualSongID) {
		Connection con = null;
		PreparedStatement stmt = null;
		
		try {
			con = getConnection();
			con.setAutoCommit(false);
			con.setCatalog(ConfigManager.get("DB_DATABASE"));
			
			String query = "SELECT P_Song_id, genre, title, band, F_User_name FROM T_Songs WHERE P_Song_id > IF((SELECT MAX(P_Song_id) FROM T_Songs) > ?,?,0) LIMIT 1;";
			
			stmt = con.prepareStatement(query);
			stmt.setInt(1, actualSongID);
			stmt.setInt(2, actualSongID);
			
			ResultSet rs = stmt.executeQuery();
			con.commit();
			
			if(rs == null) {
				return null;
			}else {
				if(rs.next()) {
					return new Song(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
				}else {
					return null;
				}
			}
		}catch(SQLException e1) {
			try {
				e1.printStackTrace();
				con.rollback();
			} catch (SQLException e) {}
			return null;
		}finally {
			if(stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {}
			}
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {}
			}
		}
	}
	
	public Song getPreviousSong(int actualSongID) {
		Connection con = null;
		PreparedStatement stmt = null;
		
		try {
			con = getConnection();
			con.setAutoCommit(false);
			con.setCatalog(ConfigManager.get("DB_DATABASE"));
			
			String query = "SELECT P_Song_id, genre, title, band, F_User_name FROM T_Songs WHERE P_Song_id < IF((SELECT MIN(P_Song_id) FROM T_Songs) < ?, ?, (SELECT MAX(P_Song_id) FROM T_Songs)+1) ORDER BY P_Song_id DESC LIMIT 1;";
			
			stmt = con.prepareStatement(query);
			stmt.setInt(1, actualSongID);
			stmt.setInt(2, actualSongID);
			
			ResultSet rs = stmt.executeQuery();
			con.commit();
			
			if(rs == null) {
				return null;
			}else {
				rs.next();
				return new Song(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
			}
		}catch(SQLException e1) {
			try {
				con.rollback();
			} catch (SQLException e) {}
			return null;
		}finally {
			if(stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {}
			}
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {}
			}
		}
	}
	
}
