package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import configuration.ConfigManager;
import logic.Song;
import logic.Vote;

public class VoteSQL  extends SQL{
	public boolean addVote(Vote vote) {
		Connection con = null;
		PreparedStatement stmt = null;
		
		try {
			con = getConnection();
			con.setAutoCommit(false);
			con.setCatalog(ConfigManager.get("DB_DATABASE"));
			
			String query = "INSERT INTO T_Votes (F_Song_id, F_User_name) VALUES (?, ?);";
			
			stmt = con.prepareStatement(query);
			stmt.setInt(1, vote.getSongID());
			stmt.setString(2, vote.getUser());
			
			stmt.execute();
			con.commit();
			return true;
		}catch(SQLException e1) {
			try {
				con.rollback();
			} catch (SQLException e) {}
			return false;
		}finally {
			if(stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {}
			}
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {}
			}
		}
	}

public ArrayList<Song> getVotedSongs() {
	Connection con = null;
	Statement stmt = null;
	
	try {
		con = getConnection();
		con.setAutoCommit(false);
		con.setCatalog(ConfigManager.get("DB_DATABASE"));
		
		String query = "SELECT songs.P_Song_id, songs.genre, songs.title, songs.band, COUNT(*) AS anzahlVotes " + 
				"FROM T_Songs AS songs " +
				"RIGHT JOIN T_Votes AS votes ON songs.P_Song_id = votes.F_Song_id " + 
				"LEFT JOIN T_Users AS users ON votes.F_User_name = users.P_User_name " + 
				"WHERE users.nobility_house IS NOT NULL " + 
				"AND votes.P_Vote_id >= coalesce( " + 
				"(SELECT votes2.P_Vote_id " + 
				"FROM T_Votes votes2 " + 
				"WHERE votes2.F_User_name = votes.F_User_name " + 
				"ORDER BY votes2.P_Vote_id DESC " + 
				"LIMIT 1 OFFSET 4 " + 
				"), 0) GROUP BY songs.P_Song_id";
		
		stmt = con.createStatement();		
		ResultSet rs = stmt.executeQuery(query);
		con.commit();
		
		if(rs == null) {
			return null;
		}else {
			ArrayList<Song> songs = new ArrayList<Song>();
			while(rs.next()) {
				songs.add(new Song(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5)));
			}
			return songs;
		}
	}catch(SQLException e1) {
		try {
			e1.printStackTrace();
			con.rollback();
		} catch (SQLException e) {}
		return null;
	}finally {
		if(stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {}
		}
		if(con != null) {
			try {
				con.close();
			} catch (SQLException e) {}
		}
	}

}
}