package database;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import configuration.ConfigManager;

public class Setup extends SQL{
	public boolean createDatabase() { //Creates database if database doesn't exists
		Connection con = null;
		Statement stmt = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			
			String query1 = "CREATE DATABASE IF NOT EXISTS "+ConfigManager.get("DB_DATABASE");
			String[] tableQuerys = new String(Files.readAllBytes(Paths.get("resources/tables.sql")), StandardCharsets.UTF_8).split(";"); //read file with CREATE TABLE statements
			
			stmt = con.createStatement();
			stmt.execute(query1);
			con.setCatalog(ConfigManager.get("DB_DATABASE")); //set current database after creating it
			
			for(int i=0;i<tableQuerys.length-1;i++) { //Excute all querys delivered via tables.sql, ignore last empty line
				stmt = con.createStatement();
				stmt.executeUpdate(tableQuerys[i]);
			}
			
			con.commit();
			
			return true;
		} catch (NumberFormatException | SQLException | IOException e) {
			try {
				con.rollback();
			} catch (Exception e1) {}
			return false;
		} finally {
			if(stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {}
			}
			if(con != null) {
				try {
					con.close();
				} catch (Exception e) {}
			}
		}
	}
	public boolean insertDemo() {
		Connection con = null;
		Statement stmt = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			
			String[] insertQuerys = new String(Files.readAllBytes(Paths.get("resources/demo.sql")), StandardCharsets.UTF_8).split(";"); //read file with INSERT statements
			
			con.setCatalog(ConfigManager.get("DB_DATABASE")); //set current database after creating it
			
			for(int i=0;i<insertQuerys.length-1;i++) { //Excute all querys delivered via demo.sql, ignore last empty line
				stmt = con.createStatement();
				stmt.executeUpdate(insertQuerys[i]);
			}
			
			con.commit();
			
			return true;
		} catch (NumberFormatException | SQLException | IOException e) {
			e.printStackTrace();
			try {
				con.rollback();
			} catch (Exception e1) {}
			return false;
		} finally {
			if(stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {}
			}
			if(con != null) {
				try {
					con.close();
				} catch (Exception e) {}
			}
		}
	}
}