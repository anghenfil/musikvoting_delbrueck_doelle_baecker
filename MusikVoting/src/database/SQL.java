package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import configuration.ConfigManager;

public abstract class SQL {
	
	public Connection getConnection() throws NumberFormatException, SQLException {
			return DriverManager.getConnection("jdbc:mysql://"+ConfigManager.get("DB_HOST")+":"+Integer.parseInt(ConfigManager.get("DB_PORT"))+"/?user="+ConfigManager.get("DB_USER")+"&password="+ConfigManager.get("DB_PASSWORD"));
	}
	
}
