 package GUI;

import java.awt.*;
import java.nio.file.Paths;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;



public class Window extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6195564832391306414L;
	
 
	private JPanel contentPane,card,addSongPanel,votingPanel,resultPanel,
	btnPanel,loginPanel,searchPanel,setupPanel,firstLoginPanel,allLoginPanel, 
	mainLoginPanel;
	private CardLayout cardLayout = new CardLayout();
	private JButton btnAddSongOPanel, btnVoting,btnReload,btnBack,btnForth;
	private String[] columnName = {"Titel","Band","Genre","Votes"};
	private DefaultTableModel tableModel = new DefaultTableModel(columnName, 0);
	public void setTableModel(DefaultTableModel tableModel) {
		this.tableModel = tableModel;
	}
	private JButton btnResult;
	private JButton btnName;
	private JButton btnSave,errorClose;
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public JButton getErrorClose() {
		return errorClose;
	}
	private JButton btnLogout;
	private JButton btnAddSong, btnChangePass;
	private JButton btnTop;
	private JButton btnFlop;
	private JTextField txtNameLogin, txtTitel, txtSinger,txtGenre,txtTitelVoting,txtSingerVoting,txtGenreVoting,txtSingersearch, txtTitelsearch;
	private JLabel lblnameLogin, lblSinger, lblTitel, lblGenre;
	private JButton btnSetup;
	private JButton btnDatabase;
	private ButtonFuctionality al;
	private JDialog dialog,firstLogin;
	private JTable resultTable;
	
	private JLabel lblTitelLogin;
	private JTextField txtTitelLogin;
	
	private JButton btnFirstSave,btnAllLogin;
	private JLabel lblFirstName,lblpassword,lblagainPassword,lblAllLogin;
	private JPasswordField passFirstLogin1,passFirstLogin2,passAllLogin;
	
	
	public JButton getBtnAddSongOPanel() {
		return btnAddSongOPanel;
	}

	public JButton getBtnReload() {
		return btnReload;
	}

	public JButton getBtnBack() {
		return btnBack;
	}

	public JButton getBtnForth() {
		return btnForth;
	}

	public String[] getColumnName() {
		return columnName;
	}

	public JTable getResultTable() {
		return resultTable;
	}
	

	public Window() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		setTitle("MusikVoting");
		try {
		    Image img = ImageIO.read(Paths.get("resources/icons/MusikVoting.png").toFile());
		    setIconImage(img);
		  } catch (Exception ex) {
		    System.out.println("loading error ("+ex+") on resources/icons/MusikVoting.png" );
		  }
		contentPane.setLayout(new BorderLayout());
		setContentPane(contentPane);
		setMinimumSize(new Dimension(450, 320));
		
		addSongPanel = new JPanel();
		votingPanel = new JPanel();
		resultPanel = new JPanel();
		searchPanel = new JPanel();
		loginPanel = new JPanel();
		
		//btnPanel
		btnPanelM();
		//btn AddSong
		btnAddSongOPanel();
		//btnVoting
		btnVotingM();
		//btnResult
		btnResultM();
		//btnName
		btnNameM();
		//btnSetup
		btnSetupM();
		
		
				
		
		
		
		//AddSongPanel
		addSongPanelM();
		//lblTitel- AddSong
		lblTitelM();
		//txtTitel- AddSong		
		txtTitelM();
		//lblSinger- AddSong
		lblSingerM();
		//txtSinger-AddSong
		txtSingerM();
		//lblGenre-AddSong
		lblGenreM();
		//txtGenre-AddSong
		txtGenreM();
		//btnAddSong-AddSong
		btnAddSongM();
		
		
		
		//VotingPanel
		votingPanelM();
		//txtGenreVoting
		txtGenreVotingM();
		//txtTitelVoting
		txtTitelVotingM();
		//txtSingerVoting
		txtSingerVotingM();
		//btnTop-Voting
		btnTopM();
		//btnBack-Voting
		btnBackM();
		//btnForth.Voting
		btnForthM();
		
		//ResultPanel
		resultPanelM();
		//resultTable
		resultTableM();
		//btnReload-Result
		btnReloadM();
		
		
		
		//Login Panel
		loginPanelM();
		//lblTitelLogin-login
		lblTitelLoginM();
		//txtTitelLogin
		txtTitelLoginM();
		//btnSave
		btnSaveM();
		//lblNameLogin
		lblNameLoginM();
		//txtNameLogin
		txtNameLoginM();
		//btnLogout
		btnLogoutM();
		
		//SetupPanel
		setupPanelM();
		//btnDataBase
		btnDatabaseM();
		//btnChangePass
		btnChangePassM();
		
		
		
		al = new ButtonFuctionality(this);

		setTableModel(tableModel);
		
		
		
		
		
		//ActionListener
		
		btnDatabase.addActionListener(al);
		btnLogout.addActionListener(al);
		btnSave.addActionListener(al);
		btnAddSongOPanel.addActionListener(al);	
		btnAddSong.addActionListener(al);
		btnResult.addActionListener(al);
		btnVoting.addActionListener(al);
		btnSetup.addActionListener(al);
		btnName.addActionListener(al);
		btnBack.addActionListener(al);
		btnForth.addActionListener(al);
		btnTop.addActionListener(al);
		btnReload.addActionListener(al);
		btnChangePass.addActionListener(al);
		
		//cardLayout
		card = new JPanel(cardLayout);
		card.add(loginPanel,"4");
		card.add(addSongPanel,"1");
		card.add(votingPanel,"2");
		card.add(resultPanel,"3");
		card.add(searchPanel,"5");
		card.add(setupPanel,"6");
		contentPane.add(card, BorderLayout.CENTER);
	}
	
	
	private void btnPanelM() {
		btnPanel = new JPanel();
		contentPane.add(btnPanel, BorderLayout.NORTH);
		GridBagLayout gbl_btnPanel = new GridBagLayout();
		btnPanel.setLayout(gbl_btnPanel);	
		btnPanel.setBackground(Color.WHITE);
	} 
	private void btnAddSongOPanel() {
		btnAddSongOPanel = new JButton("");
		btnAddSongOPanel.setContentAreaFilled(false);
		GridBagConstraints gbc_btnAddSong = new GridBagConstraints();
		gbc_btnAddSong.fill = GridBagConstraints.HORIZONTAL;
		try {
		    Image img = ImageIO.read(Paths.get("resources/icons/AddSong.png").toFile());
		    btnAddSongOPanel.setIcon(transform(img));
		  } catch (Exception ex) {
		    System.out.println("loading error ("+ex+") on resources/icons/AddSong.png" );
		  }
		gbc_btnAddSong.gridx = 0;
		gbc_btnAddSong.gridy = 0;
		gbc_btnAddSong.gridheight = 1;
		gbc_btnAddSong.gridwidth = 1;
		btnPanel.add(btnAddSongOPanel, gbc_btnAddSong);
		
	}
	private void btnVotingM() {
		btnVoting = new JButton("");
		GridBagConstraints gbc_btnVoting = new GridBagConstraints();
		btnVoting.setContentAreaFilled(false);
		gbc_btnVoting.fill = GridBagConstraints.HORIZONTAL;
		try {
		    Image img = ImageIO.read(Paths.get("resources/icons/voting.png").toFile());
		    btnVoting.setIcon(transform(img));
		  } catch (Exception ex) {
		    System.out.println("loading error ("+ex+") on resources/icons/voting.png" );
		  }
		gbc_btnVoting.gridx = 1;
		gbc_btnVoting.gridy = 0;
		btnPanel.add(btnVoting, gbc_btnVoting);
	}
	private void btnResultM() {
		btnResult = new JButton("");
		GridBagConstraints gbc_btnResult = new GridBagConstraints();
		gbc_btnResult.fill = GridBagConstraints.HORIZONTAL;
		btnResult.setContentAreaFilled(false);
		try {
		    Image img = ImageIO.read(Paths.get("resources/icons/result.png").toFile());
		    btnResult.setIcon(transform(img));
		  } catch (Exception ex) {
		    System.out.println("loading error ("+ex+") on resources/icons/result.png" );
		  }
		gbc_btnResult.gridy = 0;
		btnPanel.add(btnResult, gbc_btnResult);
	}
	private void btnSetupM() {
		btnSetup = new JButton();
		btnSetup.setIcon(new ImageIcon(Window.class.getResource("/com/sun/javafx/scene/web/skin/AlignJustified_16x16_JFX.png")));
		GridBagConstraints gbc_btnSetup = new GridBagConstraints();
		gbc_btnSetup.insets = new Insets(0,10,0,0);
		btnSetup.setContentAreaFilled(false);
		
		gbc_btnSetup.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnSetup.gridx = 5;
		gbc_btnSetup.gridy = 0;
		btnPanel.add(btnSetup, gbc_btnSetup);
	}
	private void btnNameM() {
		btnName = new JButton("  ");
		GridBagConstraints gbc_btnName = new GridBagConstraints();
		gbc_btnName.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnName.weightx = 1.5;
		btnName.setContentAreaFilled(false);
		gbc_btnName.weighty = 1;
		gbc_btnName.gridx = 3;
		gbc_btnName.gridy = 0;
		btnPanel.add(btnName, gbc_btnName);
	}
	
	
	private void addSongPanelM() {
		GridBagLayout gbl_addSongPanel = new GridBagLayout();
		addSongPanel.setLayout(gbl_addSongPanel);
		addSongPanel.setBackground(Color.WHITE);

		gbl_addSongPanel.columnWidths = new int[] {20,20,20,20};
		gbl_addSongPanel.rowHeights = new int[] {10,10,10,10,10};
	}
	private void lblTitelM() {
		lblTitel = new JLabel("Titel:");
		GridBagConstraints gbc_lblTitel = new GridBagConstraints();
		gbc_lblTitel.gridx = 0;
		gbc_lblTitel.gridy = 0;
		gbc_lblTitel.fill = GridBagConstraints.BOTH;
		gbc_lblTitel.weightx = 1;
		gbc_lblTitel.weighty = 1;
		addSongPanel.add(lblTitel, gbc_lblTitel);
	}
	private void txtTitelM() {
		txtTitel = new JTextField();
		GridBagConstraints gbc_txtTitel = new GridBagConstraints();
		gbc_txtTitel.fill = GridBagConstraints.BOTH;
		gbc_txtTitel.gridx = 1;
		gbc_txtTitel.gridy = 0;
		gbc_txtTitel.weightx = 1;
		gbc_txtTitel.weighty = 1;
		txtTitel.setColumns(10);
		addSongPanel.add(txtTitel, gbc_txtTitel);
	}
	private void lblSingerM() {
		lblSinger = new JLabel("Singer:   ");
		GridBagConstraints gbc_lblSinger = new GridBagConstraints();
		gbc_lblSinger.fill = GridBagConstraints.BOTH;
		gbc_lblSinger.gridx = 0;
		gbc_lblSinger.gridy = 1;
		gbc_lblSinger.weightx = 1;
		gbc_lblSinger.weighty = 1;
		addSongPanel.add(lblSinger, gbc_lblSinger);
	}
	private void txtSingerM() {
		txtSinger = new JTextField();
		GridBagConstraints gbc_txtSinger = new GridBagConstraints();
		gbc_txtSinger.fill = GridBagConstraints.BOTH;
		gbc_txtSinger.gridx = 1;
		gbc_txtSinger.gridy = 1;
		txtSinger.setColumns(10);
		gbc_txtSinger.weightx = 1;
		gbc_txtSinger.weighty = 1;
		addSongPanel.add(txtSinger, gbc_txtSinger);
	}
	private void lblGenreM() {
		lblGenre = new JLabel("Genre");
		GridBagConstraints gbc_lblGenre = new GridBagConstraints();
		gbc_lblGenre.fill = GridBagConstraints.BOTH;
		gbc_lblGenre.insets = new Insets(0, 0, 5, 5);
		gbc_lblGenre.gridx = 0;
		gbc_lblGenre.gridy = 2;
		gbc_lblGenre.weightx = 1;
		gbc_lblGenre.weighty = 1;
		addSongPanel.add(lblGenre, gbc_lblGenre);
	}
	private void txtGenreM() {
		txtGenre = new JTextField();
		GridBagConstraints gbc_txtGenre = new GridBagConstraints();
		gbc_txtGenre.fill = GridBagConstraints.BOTH;
		gbc_txtGenre.gridx = 1;
		gbc_txtGenre.gridy = 2;
		txtGenre.setColumns(10);
		addSongPanel.add(txtGenre, gbc_txtGenre);
	}
	private void btnAddSongM() {
		btnAddSong = new JButton();
		GridBagConstraints gbc_btnAddsong = new GridBagConstraints();
		gbc_btnAddsong.fill = GridBagConstraints.BOTH;
		try {
		    Image img = ImageIO.read(Paths.get("resources/icons/AddSong.png").toFile());
		    btnAddSong.setIcon(transform(img));
		  } catch (Exception ex) {
		    System.out.println("loading error ("+ex+") on resources/icons/AddSong.png" );
		  }
		btnAddSong.setContentAreaFilled(false);
		gbc_btnAddsong.gridx = 1;
		gbc_btnAddsong.gridy = 3;
		gbc_btnAddsong.weightx = 1;
		gbc_btnAddsong.weighty = 1;
		addSongPanel.add(btnAddSong, gbc_btnAddsong);
		}
	
	private void votingPanelM() {
		GridBagLayout gbl_votingPanel = new GridBagLayout();
		votingPanel.setLayout(gbl_votingPanel);
		votingPanel.setBackground(Color.WHITE);


		//gbl_votingPanel.columnWidths = new int[] {0,10,20,50};
		//gbl_votingPanel.rowHeights = new int[] {30,30,30,30};
	}
	private void txtGenreVotingM() {
		txtGenreVoting = new JTextField();
		txtGenreVoting.setEditable(false);
		GridBagConstraints gbc_txtGenreVoting = new GridBagConstraints();
		gbc_txtGenreVoting.gridwidth = 10;
		gbc_txtGenreVoting.fill = GridBagConstraints.BOTH;
		gbc_txtGenreVoting.gridx = 2;
		gbc_txtGenreVoting.gridy = 3;
		txtGenreVoting.setColumns(10);
		votingPanel.add(txtGenreVoting, gbc_txtGenreVoting);
	}
	private void txtTitelVotingM() {
		txtTitelVoting = new JTextField();
		txtTitelVoting.setEditable(false);
		GridBagConstraints gbc_txtTitelVoting = new GridBagConstraints();
		gbc_txtTitelVoting.gridwidth = 10;
		gbc_txtTitelVoting.fill = GridBagConstraints.BOTH;
		gbc_txtTitelVoting.gridx = 2;
		gbc_txtTitelVoting.gridy = 1;
		txtTitelVoting.setColumns(10);
		votingPanel.add(txtTitelVoting, gbc_txtTitelVoting);
	}
	private void txtSingerVotingM() {
		txtSingerVoting = new JTextField();
		txtSingerVoting.setEditable(false);
		GridBagConstraints gbc_txtSingerVoting = new GridBagConstraints();
		gbc_txtSingerVoting.gridwidth = 10;
		gbc_txtSingerVoting.fill = GridBagConstraints.BOTH;
		gbc_txtSingerVoting.gridx = 2;
		gbc_txtSingerVoting.gridy = 2;
		txtSingerVoting.setColumns(10);
		votingPanel.add(txtSingerVoting, gbc_txtSingerVoting);
	}
	private void btnTopM() {
		btnTop = new JButton();
		try {
		    Image img = ImageIO.read(Paths.get("resources/icons/Like.png").toFile());
		    btnTop.setIcon(transform(img));
		  } catch (Exception ex) {
		    System.out.println("loading error ("+ex+") on resources/icons/Like.png" );
		  }
		GridBagConstraints gbc_btnTop = new GridBagConstraints();
		btnTop.setContentAreaFilled(false);
		gbc_btnTop.gridx = 6;
		gbc_btnTop.gridy = 4;
		gbc_btnTop.fill = GridBagConstraints.BOTH;
		votingPanel.add(btnTop, gbc_btnTop);
	}
	private void btnBackM() {
		btnBack = new JButton();
		try {
		    Image img = ImageIO.read(Paths.get("resources/icons/Back.png").toFile());
		    btnBack.setIcon(transform(img));
		  } catch (Exception ex) {
		    System.out.println("loading error ("+ex+") on resources/icons/Back.png");
		  }
		GridBagConstraints gbc_btnBack = new GridBagConstraints();
		btnBack.setContentAreaFilled(false);
		gbc_btnBack.gridx = 4;
		gbc_btnBack.gridy = 4;
		gbc_btnBack.fill = GridBagConstraints.BOTH;
		votingPanel.add(btnBack,gbc_btnBack);
	}
	private void btnForthM() {
		btnForth = new JButton();
		try {
		    Image img = ImageIO.read(Paths.get("resources/icons/forth.png").toFile());
		    btnForth.setIcon(transform(img));
		  } catch (Exception ex) {
		    System.out.println("loading error ("+ex+") on resources/icons/forth.png" );
		  }
		GridBagConstraints gbc_btnForth = new GridBagConstraints();
		btnForth.setContentAreaFilled(false);
		gbc_btnForth.gridx = 5;
		gbc_btnForth.gridy = 4;
		gbc_btnForth.fill = GridBagConstraints.BOTH;
		votingPanel.add(btnForth,gbc_btnForth);	
	}
	

	private void resultPanelM() {
		resultPanel.setLayout(new BorderLayout());
		resultPanel.setBackground(Color.WHITE);
	}
	private void resultTableM() {
		resultTable = new JTable(tableModel);
		resultTable.setEnabled(false);
		resultTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		JScrollPane scrollPane = new JScrollPane(resultTable);
		resultTable.setFillsViewportHeight(true);
		resultPanel.add(scrollPane,BorderLayout.CENTER);
		
	}
	private void btnReloadM() {
		btnReload = new JButton();
		try {
		    Image img = ImageIO.read(Paths.get("resources/icons/reload.jpg").toFile());
		    btnReload.setIcon(transform(img));
		  } catch (Exception ex) {
		    System.out.println("loading error ("+ex+") on resources/icons/reload.jpg" );
		    
		  }
		resultPanel.add(btnReload, BorderLayout.NORTH);
		btnReload.setContentAreaFilled(false);
		
	}
	
	private void loginPanelM() {
		GridBagLayout gbl_loginPanel = new GridBagLayout();
		gbl_loginPanel.columnWidths = new int[] {10,50,50,50};
		gbl_loginPanel.rowHeights = new int[] {50,50,50,50,50};
		loginPanel.setBackground(Color.WHITE);
		
		loginPanel.setLayout(gbl_loginPanel);
	}
	private void lblTitelLoginM() {
		lblTitelLogin = new JLabel("House:");
		lblTitelLogin.setFont(UIManager.getFont("Button.font"));
		GridBagConstraints gbc_lblTitelLogin = new GridBagConstraints();
		gbc_lblTitelLogin.gridx = 1;
		gbc_lblTitelLogin.gridy = 1;
		gbc_lblTitelLogin.fill = GridBagConstraints.BOTH;
		loginPanel.add(lblTitelLogin, gbc_lblTitelLogin);
	}
	private void txtTitelLoginM() {
		txtTitelLogin = new JTextField("  ");
		GridBagConstraints gbc_txtTitelLogin = new GridBagConstraints();
		gbc_txtTitelLogin.fill = GridBagConstraints.BOTH;
		gbc_txtTitelLogin.weightx = 1.0;
		gbc_txtTitelLogin.weighty = 1.0;
		gbc_txtTitelLogin.gridx = 2;
		gbc_txtTitelLogin.gridy = 1;
		txtTitelLogin.setColumns(10);
		loginPanel.add(txtTitelLogin, gbc_txtTitelLogin);
	}
	private void btnSaveM() {
		btnSave = new JButton("");
		GridBagConstraints gbc_btnSave = new GridBagConstraints();
		gbc_btnSave.fill = GridBagConstraints.BOTH;
		try {
		    Image img = ImageIO.read(Paths.get("resources/icons/save.png").toFile());
		    btnSave.setIcon(transform(img));
		  } catch (Exception ex) {
		    System.out.println("loading error ("+ex+") on resources/icons/save.png" );
		  }
		btnSave.setContentAreaFilled(false);
		gbc_btnSave.gridx = 3;
		gbc_btnSave.gridy = 1;
		gbc_btnSave.weightx = 1.0;
		gbc_btnSave.weighty = 1.0;
		loginPanel.add(btnSave, gbc_btnSave);
	}
	private void lblNameLoginM() {
		lblnameLogin = new JLabel("Name:");
		GridBagConstraints gbc_lblnameLogin = new GridBagConstraints();
		gbc_lblnameLogin.fill = GridBagConstraints.BOTH;
		gbc_lblnameLogin.weightx = 1.0;
		gbc_lblnameLogin.weighty = 1.0;
		gbc_lblnameLogin.gridx = 1;
		gbc_lblnameLogin.gridy = 2;
		loginPanel.add(lblnameLogin, gbc_lblnameLogin);
	}
	private void txtNameLoginM() {
		txtNameLogin = new JTextField();
		GridBagConstraints gbc_txtNameLogin = new GridBagConstraints();
		gbc_txtNameLogin.fill = GridBagConstraints.BOTH;
		gbc_txtNameLogin.weightx = 1.0;
		gbc_txtNameLogin.weighty =1.0;
		gbc_txtNameLogin.gridx = 2;
		gbc_txtNameLogin.gridy = 2;
		loginPanel.add(txtNameLogin, gbc_txtNameLogin);
	}
	private void btnLogoutM() {
		btnLogout = new JButton("");
		GridBagConstraints gbc_btnLogout = new GridBagConstraints();
		gbc_btnLogout.fill = GridBagConstraints.BOTH;
		try {
		    Image img = ImageIO.read(Paths.get("resources/icons/logout.jpg").toFile());
		    btnLogout.setIcon(transform(img));
		  } catch (Exception ex) {
		    System.out.println("loading error ("+ex+") on resources/icons/logout.jpg" );
		  }
		btnLogout.setContentAreaFilled(false);
		gbc_btnLogout.weightx = 1.0;
		gbc_btnLogout.weighty = 1.0;
		gbc_btnLogout.gridx = 3;
		gbc_btnLogout.gridy = 2;
		loginPanel.add(btnLogout, gbc_btnLogout);
	}
	
	private void setupPanelM() {
		setupPanel = new JPanel();
		setupPanel.setLayout(new GridBagLayout());
		setupPanel.setBackground(Color.WHITE);
	}
	private void btnDatabaseM() {
		btnDatabase = new JButton("load e.g. Database");
		GridBagConstraints gbc_btnDatabase = new GridBagConstraints();
		gbc_btnDatabase.gridx = 0;
		gbc_btnDatabase.gridy = 0;
		gbc_btnDatabase.fill = GridBagConstraints.BOTH;
		setupPanel.add(btnDatabase, gbc_btnDatabase);
	}
	private void btnChangePassM() {
		btnChangePass = new JButton("change Passwort");
		GridBagConstraints gbc_btnChangePass = new GridBagConstraints();
		gbc_btnChangePass.gridx = 0;
		gbc_btnChangePass.gridy = 3;
		gbc_btnChangePass.fill = GridBagConstraints.BOTH;
		setupPanel.add(btnChangePass, gbc_btnChangePass);
	}
	
	
	public DefaultTableModel getTableModel() {
		return tableModel;
	}
	
	
	public JPanel getCard() {
		return card;
	}
	public CardLayout getCardLayout() {
		return cardLayout;
	}
	public JButton getBtnAddSong() {
		return btnAddSong;
	}
	public JButton getBtnVoting() {
		return btnVoting;
	}
	public JButton getBtnResult() {
		return btnResult;
	}
	public JButton getBtnName() {
		return btnName;
	}
	public JButton getBtnSave() {
		return btnSave;
	}
	public JButton getBtnLogout() {
		return btnLogout;
	}
	public JButton getBtnAddsong() {
		return btnAddSong;
	}
	public JButton getBtnTop() {
		return btnTop;
	}
	public JButton getBtnFlop() {
		return btnFlop;
	}
	public JTextField getTxtNameLogin() {
		return txtNameLogin;
	}
	public JTextField getTxtTitel() {
		return txtTitel;
	}
	public JTextField getTxtSinger() {
		return txtSinger;
	}
	public JTextField getTxtGenre() {
		return txtGenre;
	}
	public JTextField getTxtTitelVoting() {
		return txtTitelVoting;
	}
	public JTextField getTxtSingerVoting() {
		return txtSingerVoting;
	}
	public JTextField getTxtGenreVoting() {
		return txtGenreVoting;
	}
	public JTextField getTxtSingersearch() {
		return txtSingersearch;
	}
	public JTextField getTxtTitelsearch() {
		return txtTitelsearch;
	}

	public JButton getBtnSetup() {
		return btnSetup;
	}
	public JButton getBtnDatabase() {
		return btnDatabase;
	}
	public ButtonFuctionality getAl() {
		return al;
	}
	public JDialog getDialog() {
		return dialog;
	}
	public JButton getBtnChangePass() {
		return btnChangePass;
	}
	public JButton getBtnFirstSave() {
		return btnFirstSave;
	}
	public JButton getBtnAllLogin() {
		return btnAllLogin;
	}
	public JPasswordField getPassFirstLogin1() {
		return passFirstLogin1;
	}
	public JPasswordField getPassFirstLogin2() {
		return passFirstLogin2;
	}
	public JPasswordField getPassAllLogin() {
		return passAllLogin;
	}
	public JTextField getTxtTitelLogin() {
		return txtTitelLogin;
	}
	public void errorOutput(String error) {
		dialog = new JDialog();
		dialog.setBounds(200,200, 350, 150);
		dialog.setBackground(Color.WHITE);
		dialog.setVisible(true);
		JLabel errorNotice = new JLabel(error);
		errorClose = new JButton("Close");
		dialog.setTitle("Error");
		dialog.getContentPane().setLayout(new FlowLayout());
		dialog.getContentPane().add(errorNotice);
		dialog.getContentPane().add(errorClose);
		errorClose.addActionListener(al);
	}	
	
		public void setWhite() {
			btnAddSongOPanel.setBackground(Color.WHITE);
			btnVoting.setBackground(Color.WHITE);
			btnResult.setBackground(Color.WHITE);
			btnSetup.setBackground(Color.WHITE);
		}
		public void setSinger(String singer) {
		}
		public void setTitelLogin(String titelLogin) {
			txtTitelLogin.setText(titelLogin);
		}
		public void setBtnName(String name) {
			btnName.setText(name);;
			
		}
		public void setTxtNameLogin(String text) {
			txtNameLogin.setText(text);
			
		}
		
		public void DialogPasswordLogin(boolean first) {
			firstLogin = new JDialog();
			firstLogin.setBounds(200,200, 350, 150);
			firstLogin.setBackground(Color.WHITE);
			firstLogin.setVisible(true);
			firstLogin.getContentPane().setLayout(new BorderLayout());
			lblFirstName = new JLabel(al.getName());
			JPanel namePanel = new JPanel(new GridBagLayout());
			firstLogin.add(namePanel, BorderLayout.NORTH);
			GridBagLayout gbl_namePanel = new GridBagLayout();
			namePanel.setLayout(gbl_namePanel);
			GridBagConstraints gbc_lblFirstName = new GridBagConstraints();
			gbc_lblFirstName.fill = GridBagConstraints.BOTH;
			gbc_lblFirstName.gridx = 0;
			gbc_lblFirstName.gridy = 0;
			namePanel.add(lblFirstName,gbc_lblFirstName);
			mainLoginPanel = new JPanel();
			firstLogin.add(mainLoginPanel, BorderLayout.CENTER);
			firstLoginDialog(first);
			
		}
		private void firstLoginDialog(boolean first) {
			CardLayout cl = new CardLayout();
			mainLoginPanel.setLayout(cl);
			
			firstLoginPanel = new JPanel(new GridBagLayout());
			allLoginPanel = new JPanel(new GridBagLayout());
			
			mainLoginPanel.add(firstLoginPanel, "first");
			mainLoginPanel.add(allLoginPanel, "allLogin");
			if(first == false) { // First Login Panel
				cl.show(mainLoginPanel, "first");
				
				lblpassword = new JLabel("Password:");
				GridBagConstraints gbc_lblPassword = new GridBagConstraints();
				gbc_lblPassword.fill = GridBagConstraints.BOTH;
				gbc_lblPassword.gridx = 0;
				gbc_lblPassword.gridy = 0;
				firstLoginPanel.add(lblpassword, gbc_lblPassword);
				
				passFirstLogin1 = new JPasswordField();
				passFirstLogin1.setColumns(10);
				GridBagConstraints gbc_passFirstLogin1 = new GridBagConstraints();
				gbc_passFirstLogin1.fill = GridBagConstraints.BOTH;
				gbc_passFirstLogin1.gridx = 1;
				gbc_passFirstLogin1.gridy = 0;
				firstLoginPanel.add(passFirstLogin1,gbc_passFirstLogin1);
				
				lblagainPassword = new JLabel("Password:");
				GridBagConstraints gbc_lblagainPassword = new GridBagConstraints();
				gbc_lblagainPassword.fill = GridBagConstraints.BOTH;
				gbc_lblagainPassword.insets = new Insets(0, 0, 0, 10);
				gbc_lblagainPassword.gridx = 0;
				gbc_lblagainPassword.gridy = 1;
				firstLoginPanel.add(lblagainPassword, gbc_lblagainPassword);
				
				passFirstLogin2 = new JPasswordField();
				passFirstLogin1.setColumns(10);
				GridBagConstraints gbc_passFirstLogin2 = new GridBagConstraints();
				gbc_passFirstLogin2.fill = GridBagConstraints.BOTH;
				gbc_passFirstLogin2.gridx = 1;
				gbc_passFirstLogin2.gridy = 1;
				firstLoginPanel.add(passFirstLogin2,gbc_passFirstLogin2);
				
				
				btnFirstSave = new JButton();
				btnFirstSave .addActionListener(al);
				try {
				    Image img = ImageIO.read(Paths.get("resources/icons/save.png").toFile());
				    btnFirstSave.setIcon(transform(img));
				  } catch (Exception ex) {
				    System.out.println("loading error ("+ex+") on resources/icons/save.png" );
				  }
				GridBagConstraints gbc_btnFirstSave = new GridBagConstraints();
				gbc_btnFirstSave.fill = GridBagConstraints.BOTH;
				gbc_btnFirstSave.gridx = 1;
				gbc_btnFirstSave.gridy = 2;
				firstLoginPanel.add(btnFirstSave, gbc_btnFirstSave);
				
				
			}else {
				cl.show(mainLoginPanel, "allLogin");
				
				lblAllLogin = new JLabel("Password:");
				GridBagConstraints gbc_lblAllLogin = new GridBagConstraints();
				gbc_lblAllLogin.fill = GridBagConstraints.BOTH;
				gbc_lblAllLogin.gridx = 0;
				gbc_lblAllLogin.gridy = 0;
				gbc_lblAllLogin.insets = new Insets(0, 0, 0, 10);
				allLoginPanel.add(lblAllLogin, gbc_lblAllLogin);
				
				passAllLogin = new JPasswordField();
				passAllLogin.setColumns(10);
				GridBagConstraints gbc_AllLogin = new GridBagConstraints();
				gbc_AllLogin.fill = GridBagConstraints.BOTH;
				gbc_AllLogin.gridx = 1;
				gbc_AllLogin.gridy = 0;
				allLoginPanel.add(passAllLogin,gbc_AllLogin);
				
				btnAllLogin = new JButton();
				btnAllLogin .addActionListener(al);
				try {
				    Image img = ImageIO.read(Paths.get("resources/icons/save.png").toFile());
				    btnAllLogin.setIcon(transform(img));
				  } catch (Exception ex) {
				    System.out.println("loading error ("+ex+") on resources/icons/save.png" );
				  }
				GridBagConstraints gbc_btnAllLogin = new GridBagConstraints();
				gbc_btnAllLogin.fill = GridBagConstraints.BOTH;
				gbc_btnAllLogin.gridx = 1;
				gbc_btnAllLogin.gridy = 1;
				allLoginPanel.add(btnAllLogin, gbc_btnAllLogin);
			}
			
		} 
		
		public ImageIcon transform(Image icon) {
			Image image = icon.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
			ImageIcon endIcon = new ImageIcon(image);
			return endIcon;
		}
		
	}
