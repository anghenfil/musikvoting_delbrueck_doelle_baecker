package GUI;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

import database.SongSQL;
import database.VoteSQL;
import database.UserSQL;
import logic.PasswordHash;
import logic.Song;
import logic.Vote;
import logic.User;

public class ButtonFuctionality implements ActionListener{
	private Window window = null;
	private String user = null;
	private Song song = null;
	
	public ButtonFuctionality(Window w) {
		this.window = w;
	}
	
	public void actionPerformed(ActionEvent e) {//ActionListener
		JButton src = (JButton) e.getSource();
		if(src.equals(window.getBtnAddSongOPanel())) {//Button AddSong on btnPanel
			if(window.getBtnName().getText().equals("  ")) {
				window.getCardLayout().show(window.getCard(), "4");
			}else {
				window.getCardLayout().show(window.getCard(), "1");
				window.setWhite();
				window.getBtnAddSong().setBackground(Color.GRAY);
			}
			
			
		}else if(src.equals(window.getBtnAddSong())) {//Add Song in the Database
			Song neuerSong = new Song(window.getTxtGenre().getText() , window.getTxtSinger().getText(), window.getTxtTitel().getText());
			SongSQL songSQL1 = new SongSQL();
			songSQL1.addSong(neuerSong, user);
			window.getTxtGenre().setText("");
			window.getTxtSinger().setText("");
			window.getTxtTitel().setText("");
			
			
		}else if(src.equals(window.getBtnVoting())){//Button for Voting
			if(window.getBtnName().getText().equals("  ")) {
				window.getCardLayout().show(window.getCard(), "4");
			}else {
				window.getCardLayout().show(window.getCard(), "2");
				window.setWhite();
				window.getBtnVoting().setBackground(Color.GRAY);
				
				song = null;
				loadNextSong(); //Load first song if user clicks vote panel button
			}
			
			
		}else if(src.equals(window.getBtnResult())){//btn for see the result
			if(window.getBtnName().getText().equals("  ")) {
				window.getCardLayout().show(window.getCard(), "4");
			}else {
				window.getCardLayout().show(window.getCard(), "3");
				window.setWhite();
				window.getBtnResult().setBackground(Color.GRAY);
				
				VoteSQL playlist = new VoteSQL();
				ArrayList<Song> votedSongs = playlist.getVotedSongs();
				DefaultTableModel model = window.getTableModel();
				model.setRowCount(0);
				
				for(int i=0;i<votedSongs.size();i++) {
					model.addRow(new Object[] {votedSongs.get(i).getBand(), votedSongs.get(i).getTitle(), votedSongs.get(i).getGenre(), votedSongs.get(i).getVotes()});
				}
				
				window.getResultTable().setEnabled(true);
				//window.getResultTable().setVisible(true);
			}
			
			
		}else if(src.equals(window.getBtnName())) {//btn for login
			window.getCardLayout().show(window.getCard(), "4");
			
			
		}else if(src.equals(window.getBtnSave())) {//btn for save your Name
			if(window.getTxtNameLogin().getText().equals("  ")) {
				window.getTxtNameLogin().setBackground(Color.RED);
			}else if(window.getTxtNameLogin().getText().equals("Zehnbusch")){
				if(window.getTxtTitelLogin().getText().equals("")) {
				window.errorOutput("Herr Zennbusch \nSie wurden ausgeladen;)");
				}else{
					window.errorOutput("Herr Zehnbusch Sie sind nicht berechtig ein Titel zu haben");
				}
			}else {
				window.getTxtNameLogin().setBackground(Color.WHITE);
				window.setTitelLogin(window.getTxtTitelLogin().getText());
				if(window.getTxtTitelLogin().getText().equals("")) {
					window.setName(window.getTxtNameLogin().getText());
					user = window.getTxtNameLogin().getText();
					window.setBtnName(window.getTxtNameLogin().getText());
					User neuerUser = new User(user, null);
					UserSQL userSQL1 = new UserSQL();
					userSQL1.addUser(neuerUser);
					User check = userSQL1.checkUser(neuerUser);
					if(check != null) {
						window.DialogPasswordLogin(true);
					}else {
						window.DialogPasswordLogin(false);
					}

					window.setTxtNameLogin("  ");
					window.setTitelLogin("");
				}else {
					user = window.getTxtNameLogin().getText();
					window.setName(user);
					window.setTitelLogin(window.getTxtTitelLogin().getText());
					user = window.getTxtNameLogin().getText();
					window.setBtnName(window.getTxtTitelLogin().getText()+". "+user);
					User neuerUser = new User(user, window.getTxtTitelLogin().getText());
					UserSQL userSQL1 = new UserSQL();
					userSQL1.addUser(neuerUser);
					User check = userSQL1.checkUser(neuerUser);
					if(check != null) {
						window.DialogPasswordLogin(true);
					}else {
						window.DialogPasswordLogin(false);
					}

					window.setTxtNameLogin("  ");
					window.setTitelLogin("");
				}
			}
			
			
		}else if(src.equals(window.getBtnLogout())) {//btn for Logout
			window.getBtnName().setText("  ");
			window.getTxtNameLogin().setText("  "); 
			window.getTxtNameLogin().setBackground(Color.WHITE);
			window.setWhite();
			
			
		}else if(src.equals(window.getBtnSetup())){//btn for Setup
			if(window.getBtnName().getText().equals("  ")) {
				window.getCardLayout().show(window.getCard(), "4");
			}else {
				window.getCardLayout().show(window.getCard(), "6");
				window.setWhite();
				window.getBtnSetup().setBackground(Color.GRAY);
			}
			
			
		}else if(src.equals(window.getBtnDatabase())) {// btn for load the database
			database.Setup db = new database.Setup();
			boolean work = db.insertDemo();
			if(work == true) {
				window.getBtnDatabase().setBackground(Color.GREEN);
			}else {
				window.errorOutput("Demo Database does`t work");
			}
			
			
		}else if(src.equals(window.getErrorClose())) {//Error btn for close
			window.getDialog().setVisible(false);
			window.getDialog().dispose();
			
			
		}else if(src.equals(window.getBtnBack())) {// btn in votingPanel go back
			loadPreviousSong();
			
			
		}else if(src.equals(window.getBtnForth())){//btn in votingPanel go forward
			loadNextSong();
			
			
		}else if(src.equals(window.getBtnTop())) {//btn in Voting for Voting one Song
			if(song != null) {
				Vote newVote = new Vote(song.getID(), user);
				VoteSQL voteSQL1 = new VoteSQL();
				voteSQL1.addVote(newVote);
			}
			
			

		}else if(src.equals(window.getBtnFirstSave())) {
			/*btn in LoginPanel(Passwort JDialog)
			 * Enter Passwort for first time
			 */

			

		}else if(src.equals(window.getBtnAllLogin())) {
			/*btn in LoginPanel (Passwort JDialog)
			 * Enter Passwort for Login 
			 */

		
	}
}
	
	private void loadNextSong() {
		SongSQL songSql = new SongSQL();
		
		if(song == null) {
			song = songSql.getNextSong(0);
		}else {
			song = songSql.getNextSong(song.getID());
		}
	
		if(song != null) { //If DB returned song object update window
			window.getTxtTitelVoting().setText(song.getTitle());
			window.getTxtGenreVoting().setText(song.getGenre());
			window.getTxtSingerVoting().setText(song.getBand());
		}
	}
	
	private void loadPreviousSong() {
		SongSQL songSql = new SongSQL();
		
		if(song == null) {
			song = songSql.getPreviousSong(0);
		}else {
			song = songSql.getPreviousSong(song.getID());
		}
	
		if(song != null) { //If DB returned song object update window
			window.getTxtTitelVoting().setText(song.getTitle());
			window.getTxtGenreVoting().setText(song.getGenre());
			window.getTxtSingerVoting().setText(song.getBand());
		}
	}
	public String getName() {
		return user;
	}
}